import firebase from 'firebase/app';
import firestore from 'firebase/firestore';

var config = {
    apiKey: "AIzaSyDrFQ-hxRI7lUKu-u1O7_0R0oNN9iXiXCI",
    authDomain: "vue-chat-19759.firebaseapp.com",
    databaseURL: "https://vue-chat-19759.firebaseio.com",
    projectId: "vue-chat-19759",
    storageBucket: "",
    messagingSenderId: "319290710056",
    appId: "1:319290710056:web:f4bb0c321b793b3f"
  };
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore();