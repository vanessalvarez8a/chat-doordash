import Vue from "vue";
import Router from "vue-router";
import Username from "./views/Username.vue";
import Chat from "./views/Chat.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "username",
      component: Username
    },
    {
      path: "/chat",
      name: "chat",
      component: Chat,
      props: true,
      beforeEnter: (to, from, next) => {
        if (to.params.name) {
          next();
        } else {
          next({name: 'Username'});
        }
      }
    }
  ]
});
